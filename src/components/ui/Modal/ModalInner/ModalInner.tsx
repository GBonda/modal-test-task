import React, { ChangeEvent, FC, useEffect, useState } from "react";
import { modalContent } from "../../../../utils/constants/modalContent";
import { CheckboxProps } from "../../../form/CheckBox/CheckBox";
import { CheckboxGroup } from "../../../form/CheckboxGroup/CheckboxGroup";
import { CustomInput } from "../../../form/CustomInput/CustomInput";
import { ModalVariants } from "../Modal";
import styles from "./modal-inner.module.scss";

type ModalInnerProps = {
  variant: ModalVariants;
  options: CheckboxProps[];
};

export const ModalInner: FC<ModalInnerProps> = ({ variant, options }) => {
  const subTitles = {
    [ModalVariants.SELECT_MEMBERS]: modalContent.selectMembersSubtitle,
    [ModalVariants.SELECT_INTEGRATIONS]:
      modalContent.selectIntegrationsSubtitle,
  };

  const searchPlaceholder = {
    [ModalVariants.SELECT_MEMBERS]: modalContent.membersPlaceholder,
    [ModalVariants.SELECT_INTEGRATIONS]: modalContent.integrationsPlaceholder,
  };

  const searchLabel = {
    [ModalVariants.SELECT_MEMBERS]: modalContent.membersLabel,
    [ModalVariants.SELECT_INTEGRATIONS]: modalContent.integrationsLabel,
  };

  const [checkedList, setCheckedList] = useState(options);
  const [filter, setFilter] = useState("");

  const onChange = (list: CheckboxProps[]) => {
    setCheckedList(list);
  };

  const filterOptions = (event: ChangeEvent<HTMLInputElement>) => {
    setFilter(event.target.value);
  };

  useEffect(() => {
    setCheckedList(options);
  }, [options]);

  return (
    <div className={styles.modalInner}>
      {variant !== ModalVariants.CREATE_CHANNEL ? (
        <>
          <p className={styles.subtitle}>{subTitles[variant]}</p>
          <CustomInput
            itemName={
              variant === ModalVariants.SELECT_MEMBERS
                ? "searchMember"
                : "serachTool"
            }
            placeholder={searchPlaceholder[variant]}
            label={searchLabel[variant]}
            withIcon
            onChange={filterOptions}
          />
          <CheckboxGroup
            options={checkedList}
            onChange={onChange}
            filter={filter}
          />
        </>
      ) : (
        <>
          <div className={styles.emptyBlock} />
          <p className={styles.description}>
            Collaborate with team members with powerful developer tooling
            integrations.
          </p>
          <div className={styles.inputsWrapper}>
            <CustomInput
              placeholder="Name"
              label="Channel Name"
              itemName="name"
              rules={[{ required: true, message: "Please input chanel name" }]}
            />
            <CustomInput
              itemName="description"
              placeholder="Optional channel description"
              label="Description"
            />
          </div>
        </>
      )}
      {/* <SearchInput placeholder="Search members by name or email..." /> */}
      {/* <CustomCheckbox
        title="Tejas Ravishankar"
        subtitle="tejas@dimension.dev"
      /> */}
    </div>
  );
};
