import { Form } from "antd";
import { useForm } from "antd/es/form/Form";
import React, { FC, useState } from "react";
import { members } from "../../../utils/constants/members";
import { tools } from "../../../utils/constants/tools";
import { CustomButton } from "../../form/Button/CustomButton";
import { Link } from "../../icons/Link/Link";
import styles from "./modal.module.scss";
import { ModalInner } from "./ModalInner/ModalInner";

export enum ModalVariants {
  CREATE_CHANNEL = "Create a Channel",
  SELECT_MEMBERS = "Select Members",
  SELECT_INTEGRATIONS = "Select Integrations",
}

type FormValues = {
[x: string]: string;
}

export const Modal: FC = () => {
  const [variant, setVariant] = useState(ModalVariants.CREATE_CHANNEL);

  const onFinish = (values: FormValues) => {
    switch (variant) {
      case ModalVariants.CREATE_CHANNEL: {
        setVariant(ModalVariants.SELECT_MEMBERS);
        break;
      }
      case ModalVariants.SELECT_MEMBERS: {
        setVariant(ModalVariants.SELECT_INTEGRATIONS);
        break;
      }
      default:
        break;
    }
  }

  const [form] = Form.useForm();

  return (
    <div
      className={styles.modal}
    >
      <h2>{variant}</h2>
      <Form onFinish={onFinish} form={form}>
        <ModalInner variant={variant} options={variant === ModalVariants.SELECT_MEMBERS ? members : tools} />
        <div className={styles.modalActions}>
          <a href="#" className={styles.modalLink}>
            Learn more about channels
            <Link />
          </a>
          <CustomButton title="Next" />
        </div>
      </Form>
    </div>
  );
};
