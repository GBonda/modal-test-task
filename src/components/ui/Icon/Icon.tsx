import React, { FC } from "react";
import styles from "./icon.module.scss";

type IconProps = { color?: string; src: string };

export const Icon: FC<IconProps> = ({ color, src }) => {
  return (
    <div style={{ backgroundColor: color }} className={styles.iconWrapper}>
      <img alt="" src={src} />
    </div>
  );
};
