import { Form } from "antd";
import React, { FC } from "react";
import { CheckboxProps, CustomCheckbox } from "../CheckBox/CheckBox";
import styles from "./checkbox-group.module.scss";

type CheckboxGroupProps = {
  options: CheckboxProps[];
  onChange: (list: CheckboxProps[]) => void;
  filter?: string;
};

export const CheckboxGroup: FC<CheckboxGroupProps> = ({
  options,
  onChange,
  filter,
}) => {
  const filterList = (list: CheckboxProps[]) => {
    if (!filter) return list;
    return list.filter(
      (item) =>
        item.title.toLowerCase().includes(filter.toLowerCase()) ||
        item.subtitle.toLowerCase().includes(filter.toLowerCase())
    );
  };
  const toggleCheckBox = (title: string) => {
    onChange(
      options.map((item) =>
        item.title === title ? { ...item, checked: !item.checked } : item
      )
    );
  };
  return (
    <div className={styles.groupWrapper}>
      {filterList(options).map(({ title, subtitle, checked, img }) => {
        return (
          <CustomCheckbox
            title={title}
            subtitle={subtitle}
            key={title}
            checked={checked}
            toggleChecked={() => toggleCheckBox(title)}
            img={img}
          />
        );
      })}
    </div>
  );
};
