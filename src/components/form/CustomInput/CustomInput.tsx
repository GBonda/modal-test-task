import { Form, Input } from "antd";
import { Rule } from "antd/es/form";
import classNames from "classnames";
import React, { ChangeEvent, FC } from "react";
import { SearchIcon } from "../../icons/Search/SearchIcon";
import styles from "./custom-input.module.scss";

type CustomInputProps = {
  placeholder?: string;
  label: string;
  withIcon?: boolean;
  itemName: string;
  rules?: Rule[];
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void
};

export const CustomInput: FC<CustomInputProps> = ({
  placeholder,
  label,
  withIcon,
  itemName,
  rules,
  onChange
}) => {
  const inputInnerWrapperStyles = classNames(
    styles.inputInnerWrapper,
    withIcon && styles.searchInput
  );

  return (
    <label className={styles.inputWrapper}>
      <p className={styles.inputLabel}>{label}</p>
      <div className={inputInnerWrapperStyles}>
        {withIcon && (
          <span className={styles.searchIcon}>
            <SearchIcon />
          </span>
        )}
        <Form.Item name={itemName} style={{ position: "unset", marginBottom: '0' }} rules={rules}>
          <Input placeholder={placeholder} onChange={onChange}/>
        </Form.Item>
      </div>
    </label>
  );
};
