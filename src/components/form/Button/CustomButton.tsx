import React, { FC } from "react";
import { Enter } from "../../icons/Enter/Enter";
// import { ButtonStyled, IconSpan } from "./Button.styled";
import styles from "./button.module.scss";
import { Button } from "antd";

type ButtonProps = {
  title: string;
};

export const CustomButton: FC<ButtonProps> = ({ title }) => {
  return (
    <Button className={styles.button} type="ghost"  htmlType="submit">
      {title}
      <span className={styles.icon}>
        <Enter />
      </span>
    </Button>
  );
};
