import { Checkbox } from "antd";
import classNames from "classnames";
import React, { FC, PropsWithChildren, ReactNode, useState } from "react";
import { Checked } from "../../icons/Checked/Checked";
import styles from "./checkbox.module.scss";

export type CheckboxProps = {
  title: string;
  subtitle: string;
  checked: boolean;
  img: ReactNode;
};

export type CustomCheckboxProps = CheckboxProps & {
  toggleChecked: () => void;
  img: string | ReactNode;
};

export const CustomCheckbox: FC<PropsWithChildren<CustomCheckboxProps>> = ({
  children,
  title,
  subtitle,
  checked,
  toggleChecked,
  img,
}) => {
  // const [checked, setChecked] = useState(true);

  // const toggleChecked = () => {
  //   setChecked(!checked);
  // };

  const checkboxStyles = classNames(
    styles.checkboxWrapper,
    checked && styles.checkboxChecked
  );

  return (
    <div className={checkboxStyles}>
      <div className={styles.media}>
        {typeof img === "string" ? <img alt="" src={img} /> : img}
      </div>
      <Checkbox checked={checked} onChange={toggleChecked}>
        <div>
          <p>{title}</p>
          <p>{subtitle}</p>
        </div>
        {children}
        <span className={styles.checkboxIcon}>{checked && <Checked />}</span>
      </Checkbox>
    </div>
  );
};
