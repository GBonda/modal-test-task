export const modalContent = {
    selectMembersSubtitle: "Select members you want to collaborate with in this channel.",
    selectIntegrationsSubtitle: "Select the tools you want to integrate with this channel.",
    membersPlaceholder: "Search members by name or email...",
    integrationsPlaceholder: "Search integrations...",
    membersLabel: 'Members',
    integrationsLabel: 'Integrations',
}