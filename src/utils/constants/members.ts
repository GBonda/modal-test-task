export const members = [
  {
    title: "Tejas Ravishankar",
    subtitle: "tejas@dimension.dev",
    checked: false,
    img: "/images/semi-cropped 1.png",
  },
  {
    title: "Conrad Crawford",
    subtitle: "conrad@dimension.dev",
    checked: false,
    img: "/images/semi-cropped 2.png",
  },
  {
    title: "Aiden Bai",
    subtitle: "aiden@dimension.dev",
    checked: false,
    img: "/images/semi-cropped 3.png",
  },
  {
    title: "Tom Preston-Werner",
    subtitle: "tom@dimension.dev",
    checked: false,
    img: "/images/semi-cropped 4.png",
  },
];
