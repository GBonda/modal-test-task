import { Icon } from "../../components/ui/Icon/Icon";

export const tools = [
  {
    title: "Linear",
    subtitle: "View, edit, and get notified about issues.",
    checked: false,
    img: <Icon src="/images/linear-company-icon 1.svg" color="transparent"/>,
  },
  {
    title: "Vercel",
    subtitle: "View, edit and get notified about your deployments.",
    checked: false,
    img: <Icon src="/images/logos_vercel-icon.svg" color="#000000"/>,
  },
  {
    title: "Netlify",
    subtitle: "View, edit and get notified about your deployments.",
    checked: false,
    img: <Icon src="/images/logomark-light 1.svg" color="#000000"/>,
  },
  {
    title: "Figma",
    subtitle: "Preview your Figma files.",
    checked: false,
    img: <Icon src="/images/logos_figma.svg" />,
  },
  {
    title: "Google Meet",
    subtitle: "Create, schedule, and join meetings.",
    checked: false,
    img: <Icon src="/images/logos_google-meet.svg" />,
  },
  {
    title: "Zoom",
    subtitle: "Create, schedule, and join meetings.",
    checked: false,
    img: <Icon src="/images/akar-icons_zoom-fill.svg" color="transparent" />
  },
];
