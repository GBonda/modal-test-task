import React from "react";
import { Modal } from "./components/ui/Modal/Modal";
import "./App.css";

function App() {
  return (
    <div className="App" style={{ padding: "50px" }}>
      <Modal />
    </div>
  );
}

export default App;
